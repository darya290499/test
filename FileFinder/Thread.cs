﻿using System;

using System.Windows.Forms;
using System.IO;
using System.ComponentModel;

using System.Text.RegularExpressions;
namespace FileFinder
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm
	{
		object filesCount = 0;//количество проверенных файлов
		string current = "";//имя проверяемого файла
		Regex patternText;//шаблон имени файла
		static string startingDirectory = "";//стартовая директория
		static string innerText = "";//текст внутри
		
		void ThreadRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if(e.Error != null)
			{
				StopClick(sender, e);
				MessageBox.Show("В процессе поиска возникла ошибка\n" + e.Error.Message);
			}
			else 
			if(!e.Cancelled)
			{
				StopClick(sender, e);
				MessageBox.Show("Поиск завершен");
			}
			else
				MessageBox.Show("Поиск остановлен");
		}
		

		void ThreadDoWork(object sender, DoWorkEventArgs e) //запускается после RunWorkerAsync()
		{
			
			SearchInDirectory(new DirectoryInfo(directory.Text), e);
			//
			me.WaitOne(); //проверка на паузу
			if (thread.CancellationPending)
            	{
					e.Cancel = true;
	            	return;
				}
		}
		
		void SearchInDirectory(DirectoryInfo dir, DoWorkEventArgs e)
		{
			var files = dir.GetFiles();//список всех файлов заданной директории
			SearchForFiles(files, e);//поиск файла по списку
			var directories = dir.GetDirectories();//список всех подкаталогов
			foreach(var directory in directories)//??
			{
				me.WaitOne();
				if (thread.CancellationPending)
				{
					e.Cancel = true;
	            	return;
				}
				SearchInDirectory(directory, e);
			}
		}
		
		void RefreshData(FileInfo file)//обновление данных по файлам
		{
			lock(filesCount) //блокировка от использования другими потоками
			{
				filesCount = (int)filesCount + 1;//количество файлов
			}
			lock(current)
			{
				current = file.Name;//имя проверяемого файла
			}
		}
		
		void SearchForFiles(FileInfo[] files, DoWorkEventArgs e)
		{
			
			foreach(var file in files)
			{

				me.WaitOne();
				if (thread.CancellationPending)
	            {
					e.Cancel = true;
	            	return;
	            }
				RefreshData(file);
				
				var matches = patternText.Matches(file.Name);
				if (matches.Count > 0)
				{
					//try 
		        	{
						using (StreamReader str = new StreamReader(file.Directory.FullName+'\\' + file.Name) )
			            {//???
			                while (!str.EndOfStream) //пока не конец файла
				            {
								                	
				                string st = str.ReadLine();
				                lock(innerText)
				                {
					                if (st.IndexOf(innerText) > -1)
					                {
					                	thread.ReportProgress(0,file);
					                	break;
					                }
				                }
				            }
			            }
		        	}
					//catch (Exception e) 
			        {
			           // MessageBox.Show(e.Message);
			        }
				}
		        
			}
		}
		
		
	}
}