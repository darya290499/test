﻿
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
//using System.Linq;

namespace FileFinder
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{

		bool paused = true;
		

		Stopwatch elaps = new Stopwatch();
		
		ManualResetEvent me = new ManualResetEvent(true);
		
		public MainForm()
		{
			InitializeComponent();
			
			if(File.Exists("settings.dat"))
			{
				using (var FileStream = File.OpenRead("settings.dat"))
				{
					var serializer = new BinaryFormatter();
					directory.Text = (string)serializer.Deserialize(FileStream);
					pattern.Text = (string)serializer.Deserialize(FileStream);
					inner.Text = (string)serializer.Deserialize(FileStream);
				}
			}

		}
		
		int NodeFind(TreeNodeCollection nodes, string text) //найти в текущем уровне дерева нужную папку
		{
			for(int i=0; i<nodes.Count; i++)
			{
				var node = nodes[i] as TreeNode;
				if(node.Text == text)
				{
					return i;
                }
			}
			return -1;
		}
		
		void ThreadProgressChanged(object sender, ProgressChangedEventArgs e) //вызывается после ReportProgress()
		{
			var path = new Stack<string>();
			var file = e.UserState as FileInfo; //файл, который мы нашли
			for(var parent = file.Directory; parent != null; parent = parent.Parent)
				path.Push(parent.Name);
			
			var nodes = explorer.Nodes;
			TreeNode n = null;
			while(path.Count>0) //пока есть элементы в стеке
			{
				int found = NodeFind(nodes, path.Peek()); //находим на текущем уровне нужную папку по имени из стэка
				
				if(found == -1) //если не нашли
				{
					while(path.Count>0)
					{
						n = new TreeNode(path.Pop());
						nodes.Add(n);
                        nodes = n.Nodes;
                    }
                    nodes = n.Nodes;
                    nodes.Add(file.Name);
					return;
				}
				else
				{
					nodes = nodes[found].Nodes;
					path.Pop();
				}
			}
			nodes.Add(file.Name);
			
		}
		
		void ChooseClick(object sender, EventArgs e)
		{
            FolderBrowserDialog Dialog = new FolderBrowserDialog();
            Dialog.Description = "Выбор директории";

            Dialog.SelectedPath = @"C:\";
    
            if (Dialog.ShowDialog() == DialogResult.OK)
            {
                directory.Text = Dialog.SelectedPath;
            }
		}
		

		
		void MainFormFormClosed(object sender, FormClosedEventArgs e)
		{
			using (var FileStream = File.Create("settings.dat")) //save all text fields
			{
				var serializer = new BinaryFormatter();
				serializer.Serialize(FileStream, directory.Text);
				serializer.Serialize(FileStream, pattern.Text);
				serializer.Serialize(FileStream, inner.Text);
			}
		}
		
		void StartStopClick(object sender, EventArgs e)
		{
			explorer.Nodes.Clear();
			clean.Enabled = false;
			if(paused)
			{
				if(!thread.IsBusy)
				{
					filesCount = 0;
					
					startingDirectory = directory.Text;
					patternText = new Regex(pattern.Text);
					innerText = inner.Text;
					
					thread.RunWorkerAsync();
				}
				elaps.Start();
				refresher.Start();
				me.Set();
				StartStop.Text = "Pause";
				stop.Enabled = true;
			}
			else
			{
				elaps.Stop();
				refresher.Stop();
				me.Reset();
				StartStop.Text = "Start";
			}
			RefresherTick(sender, e);
			paused = !paused;
		}
		
		void StopClick(object sender, EventArgs e)
		{
			clean.Enabled = true;
			RefresherTick(sender, e);
			refresher.Stop();
			elaps.Stop();
			elaps.Reset();
			stop.Enabled = false;
			me.Set();
			paused = true;
			StartStop.Text = "Start";
			
			thread.CancelAsync();
		}
		
		void RefresherTick(object sender, EventArgs e)
		{
			time.Text = "elapsed time: " + elaps.ElapsedMilliseconds.ToString() + " ms";
			lock(current)
			{
				currentFile.Text = "current file: " + current.ToString();
			}
			lock(filesCount)
			{
				processed.Text = "processed files: " + filesCount.ToString();
			}
		}
		
		void CleanClick(object sender, EventArgs e)
		{
			explorer.Nodes.Clear();
		}

	}
}
