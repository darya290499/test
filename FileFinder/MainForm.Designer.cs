﻿/*
 * Created by SharpDevelop.
 * User: godlike
 * Date: 9/13/2019
 * Time: 20:21
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace FileFinder
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TreeView explorer;
		private System.Windows.Forms.TextBox directory;
		private System.Windows.Forms.Button choose;
		private System.Windows.Forms.TextBox pattern;
		private System.Windows.Forms.TextBox inner;
		private System.Windows.Forms.Label currentFile;
		private System.Windows.Forms.Label time;
		private System.Windows.Forms.Button StartStop;
		private System.Windows.Forms.Label processed;
		private System.Windows.Forms.Button stop;
		private System.Windows.Forms.Timer refresher;
		private System.ComponentModel.BackgroundWorker thread;
		private System.Windows.Forms.Button clean;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.explorer = new System.Windows.Forms.TreeView();
			this.directory = new System.Windows.Forms.TextBox();
			this.choose = new System.Windows.Forms.Button();
			this.pattern = new System.Windows.Forms.TextBox();
			this.inner = new System.Windows.Forms.TextBox();
			this.currentFile = new System.Windows.Forms.Label();
			this.processed = new System.Windows.Forms.Label();
			this.time = new System.Windows.Forms.Label();
			this.StartStop = new System.Windows.Forms.Button();
			this.stop = new System.Windows.Forms.Button();
			this.refresher = new System.Windows.Forms.Timer(this.components);
			this.thread = new System.ComponentModel.BackgroundWorker();
			this.clean = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// explorer
			// 
			this.explorer.Location = new System.Drawing.Point(12, 23);
			this.explorer.Name = "explorer";
			this.explorer.Size = new System.Drawing.Size(496, 478);
			this.explorer.TabIndex = 0;
			// 
			// directory
			// 
			this.directory.Enabled = false;
			this.directory.Location = new System.Drawing.Point(526, 67);
			this.directory.Name = "directory";
			this.directory.Size = new System.Drawing.Size(238, 22);
			this.directory.TabIndex = 1;
			// 
			// choose
			// 
			this.choose.Location = new System.Drawing.Point(634, 23);
			this.choose.Name = "choose";
			this.choose.Size = new System.Drawing.Size(130, 28);
			this.choose.TabIndex = 2;
			this.choose.Text = "choose directory";
			this.choose.UseVisualStyleBackColor = true;
			this.choose.Click += new System.EventHandler(this.ChooseClick);
			// 
			// pattern
			// 
			this.pattern.Location = new System.Drawing.Point(525, 142);
			this.pattern.Name = "pattern";
			this.pattern.Size = new System.Drawing.Size(238, 22);
			this.pattern.TabIndex = 3;
			// 
			// inner
			// 
			this.inner.Location = new System.Drawing.Point(526, 205);
			this.inner.Name = "inner";
			this.inner.Size = new System.Drawing.Size(238, 22);
			this.inner.TabIndex = 4;
			// 
			// currentFile
			// 
			this.currentFile.Location = new System.Drawing.Point(525, 272);
			this.currentFile.Name = "currentFile";
			this.currentFile.Size = new System.Drawing.Size(238, 23);
			this.currentFile.TabIndex = 5;
			this.currentFile.Text = "current file: ";
			// 
			// processed
			// 
			this.processed.Location = new System.Drawing.Point(525, 308);
			this.processed.Name = "processed";
			this.processed.Size = new System.Drawing.Size(238, 23);
			this.processed.TabIndex = 6;
			this.processed.Text = "processed files:";
			// 
			// time
			// 
			this.time.Location = new System.Drawing.Point(526, 347);
			this.time.Name = "time";
			this.time.Size = new System.Drawing.Size(238, 23);
			this.time.TabIndex = 7;
			this.time.Text = "elapsed time:";
			// 
			// StartStop
			// 
			this.StartStop.Location = new System.Drawing.Point(526, 468);
			this.StartStop.Name = "StartStop";
			this.StartStop.Size = new System.Drawing.Size(75, 33);
			this.StartStop.TabIndex = 8;
			this.StartStop.Text = "Start";
			this.StartStop.UseVisualStyleBackColor = true;
			this.StartStop.Click += new System.EventHandler(this.StartStopClick);
			// 
			// stop
			// 
			this.stop.Enabled = false;
			this.stop.Location = new System.Drawing.Point(607, 468);
			this.stop.Name = "stop";
			this.stop.Size = new System.Drawing.Size(75, 33);
			this.stop.TabIndex = 9;
			this.stop.Text = "Stop";
			this.stop.UseVisualStyleBackColor = true;
			this.stop.Click += new System.EventHandler(this.StopClick);
			// 
			// refresher
			// 
			this.refresher.Interval = 10;
			this.refresher.Tick += new System.EventHandler(this.RefresherTick);
			// 
			// thread
			// 
			this.thread.WorkerReportsProgress = true;
			this.thread.WorkerSupportsCancellation = true;
			this.thread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ThreadDoWork);
			this.thread.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.ThreadProgressChanged);
			this.thread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ThreadRunWorkerCompleted);
			// 
			// clean
			// 
			this.clean.Location = new System.Drawing.Point(688, 468);
			this.clean.Name = "clean";
			this.clean.Size = new System.Drawing.Size(75, 33);
			this.clean.TabIndex = 10;
			this.clean.Text = "Clean";
			this.clean.UseVisualStyleBackColor = true;
			this.clean.Click += new System.EventHandler(this.CleanClick);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(525, 179);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(238, 23);
			this.label1.TabIndex = 11;
			this.label1.Text = "inner text";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(526, 116);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(238, 23);
			this.label2.TabIndex = 12;
			this.label2.Text = "name pattern";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(776, 516);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.clean);
			this.Controls.Add(this.stop);
			this.Controls.Add(this.StartStop);
			this.Controls.Add(this.time);
			this.Controls.Add(this.processed);
			this.Controls.Add(this.currentFile);
			this.Controls.Add(this.inner);
			this.Controls.Add(this.pattern);
			this.Controls.Add(this.choose);
			this.Controls.Add(this.directory);
			this.Controls.Add(this.explorer);
			this.Name = "MainForm";
			this.Text = "FileFinder";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFormFormClosed);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
